describe('DOMService',function(){
  beforeEach(module('DOMServiceModule'));

  var DOMService;

  beforeEach(inject(function(_DOMService_){
    DOMService = _DOMService_;
  }));

  describe('resizeComponent',function(){


    it('resize given element by especif ratio',function(){
      var component = angular.element("<div></div>");

      component.css({
        'height':'200px',
        'width': '400px'
      });

      var ratio = 2;

      var resizedComponent = DOMService.resizeComponent(component, ratio);

      expect(resizedComponent[0].style.height).toEqual('400px');
      expect(resizedComponent[0].style.width).toEqual('800px');

    });


  });

});
