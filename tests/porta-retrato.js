describe('portaRetrato', function() {

  describe("frame's content", function() {
    var $compile, $rootScope, content, element;

    beforeEach(module('portaRetrato'));


    beforeEach(inject(function(_$compile_, _$rootScope_) {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
    }));

    beforeEach(function() {
      element = $compile("<porta-retrato><div>Hi</div></porta-retrato>")($rootScope);
      $rootScope.$digest();

      content = element.find('div');

      element.css({height: '200px', width: '400px',});
      content.css({height: '50px', width: '2px',});

      $rootScope.$digest();
    });

    it('has width and height larger the his parent', function() {
      var contentHeightIsLarger = rpx(content.css('height')) >= rpx(element.css('height'));
      var contentWidthIsLarger = rpx(content.css('width')) >= rpx(element.css('width'));

      expect(contentHeightIsLarger).toBeTruthy();
      expect(contentWidthIsLarger).toBeTruthy();
    });

    it('is centralized',function(){
      expect(rpx(content.css('top')) === -4900).toBeTruthy();
      expect(rpx(content.css('left')) === 0).toBeTruthy();
    });
  });

  function rpx(pixels) {
    return Number(pixels.replace(/px/, ''));
  }
});
