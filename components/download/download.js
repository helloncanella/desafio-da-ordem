angular.module('downloadModule',[])

.directive('download', function(){
  return {
    restrict: 'C',
    controllerAs: 'ctrl',
    controller: function(){
      var userAgent = navigator.userAgent;  
      
      this.buttons = {
        'apple':false,
        'android': false
      }
      
      if( /Android/i.test(userAgent) ) {
        this.buttons['android'] = true;
      }else if ( /iPad|iPhone/i.test(userAgent) ) {
        this.buttons['apple'] = true; 
      }
    }
  }
})