angular.module('cards',[])

.controller('CardsController',['$scope', '$http', '_', function($scope,$http,_){

  $scope.selectedCategories = [] ;

  getAllPosts().then(function(posts){

    $scope.allPosts = posts.data;
    $scope.allPosts.forEach(searchEditorial);

    var choosenPosts = $scope.allPosts.slice(0,6);
    $scope.posts = choosenPosts;


    console.log(posts.data[0]);

  });

  getAllImages().then(function(images){

    $scope.images = [];

    images.data.forEach(function(image, index){
      var post = image.post;
      var url = image.guid;
      $scope.images[post] = url.rendered;
    });

    $scope.$broadcast('imagesReady');

  });


  $scope.search = function(criteria,searchContent){
    if(criteria=='category'){
      var categories = [];

      $scope.expression = '';

      searchContent.forEach(function(current, index){
        if(current===true){
          categories.push(index);
        }
      });

      if(!$scope.allPosts){
        getAllPosts().then(function(posts){
          console.log(posts.data);
          $scope.allPosts = posts.data;
          $scope.posts = getPostsByCategories(categories);
        });
      }else{
        $scope.posts = getPostsByCategories(categories);
      }


    }else{
      $scope.selectedCategories = [] ;
      $http.get('/wp-json/wp/v2/posts?search='+searchContent).success(function(response){
        $scope.posts = response;
        $scope.posts.forEach(searchEditorial);
      });
    }




    function getPostsByCategories(selectedCategories){

      var posts =
        $scope.allPosts.filter(function(post, index){
          var interceptionLength = _.intersection(post.categories, selectedCategories).length;
          var selectedLenth = selectedCategories.length;

          return interceptionLength == selectedLenth;
        });

      return posts;
    }
  };


  $scope.click = function(categoryId){
    $scope.selectedCategories[categoryId] = !$scope.selectedCategories[categoryId];
  };


  function getAllPosts(){
    return $http.get('/wp-json/wp/v2/posts');
  }

  function getAllImages(){
    return $http.get('/wp-json/wp/v2/media');
  }


  //id of editorial tag in the wordpress's database
  var EDITORIAL = 17;

  function searchEditorial (article, index, posts){
    if(article.tags.indexOf(EDITORIAL) !== -1){
      article.editorial = true;

      if(!posts[0].editorial){
        _.move(posts, index,0);
      }

    }
  }

}])

.directive('categories',function(){
  return{
    restrict:'E',
    controller: ["$http", "$scope", function($http, $scope){
      $scope.selected = false;

      $http.get('wp-json/wp/v2/categories').success(function(response){
        var categories = [];

        response.forEach(function(each){
          categories.push({name: each.name, id: each.id});
        });

        $scope.categories = categories;

      });
    }]
  };
})

.directive('pictureFrame', function(){
  return{
    restrict: 'E',
    scope: {
      src:"&"
    },
    link: function(scope, element, attr){
      setBackground();

      scope.$on('imagesReady', function(){
        setBackground();
      });

      function setBackground(){
        console.log(scope.src());    
         element.css({
           'background': 'url('+scope.src()+')',
           'background-size': 'cover'
         }); 
      }
    }
  };
})

.directive('masonry', ['$window', function($window){
  return{
    restrict:'E',
    link: function(scope, element){
      
      scope.$on('activeMansory', function(){        

        var masonry = new Masonry(element[0], {
          itemSelector: '.item',
          columnWidth: '.grid-sizer',
          gutter: '.gutter',
          percentPosition: true
        });
       

        masonry.reloadItems();
      })


    }
  };
}])


.directive('monitor', ['$timeout', function($timeout){

  return{
    link:function(scope, element){
     
      if(scope.$last === true){
        $timeout(function(){
          scope.$emit('activeMansory');
        },600)
      }


      scope.$watch("posts.length", function(newvalue,oldvalue){
        if(newvalue!==oldvalue){
          scope.$emit('activeMansory');
        } 
      }, true); 
      
    }
  }
  
}])

;
