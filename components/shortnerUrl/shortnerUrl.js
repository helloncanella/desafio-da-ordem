angular.module('shortnerUrl',[])

.factory('shortUrl', ['$http', function($http) { 
  return function(apiKey, url) {
    return $http({
      method: 'POST',
      url: 'https://www.googleapis.com/urlshortener/v1/url?key=' + apiKey,
      contentType: 'application/json; charset=utf-8',
      data: {
        "longUrl": url
      }
    })
  }
}])