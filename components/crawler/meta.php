<?php
        function curl($url){
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $output = curl_exec($ch);
                curl_close($ch);

                return json_decode($output);
        }


        function getImage($postId){
                $url = 'http://159.203.77.227/wp-json/wp/v2/media?parent='.$postId;
                $json = curl($url);
                $image = $json[0]->guid->rendered;

                return $image;

        }

        function getPost($id){
                $url =  "http://159.203.77.227/wp-json/wp/v2/posts/".$id;
                $post = curl($url);

                return $post;

        }


        function head($id){

                $post  = getPost($id);
                $image = getImage($id);

                $head = '<meta charset="UTF-8">'.PHP_EOL;
                $head .= '<meta property="og:url" content="http://desafiodaordem.com.br/noticia/'.($post->slug).'/'.($post->id).'"/>'.PHP_EOL;
                $head .= '<meta property="og:title" content="'.($post->title->rendered).'" />'.PHP_EOL;
                $head .= '<meta property="og:image" content="'.$image.'" />'.PHP_EOL;
                $head .= '<meta property="og:description" content="Desafio da Ordem" />'.PHP_EOL;
                $head .= '<meta property="og:type"               content="article" />'.PHP_EOL;

                return $head;

        }

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <?php echo head($_GET['postId']) ?>
</head>
<body>

</body>
</html>

 
