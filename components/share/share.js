angular.module('shareModule', ['shortnerUrl'])

.directive('share', ['$document', '$window', '$filter', 'shortUrl', function($document, $window, $filter, shortUrl) {
  return {
    restrict: 'E',
    controllerAs: 'shareCtrl',
    scope: true,
    bindToController: {
      title: "@",
      url: "@",
      relativeUrl: "@",
    },
    controller: function() {

      var self = this,
        url = getURL(),
        title = this.title,
        escape = $filter('escape');


      this.click = function(socialNetwork) {
        var shortenUrl,
          socialNetworksURL = {
            'facebook': 'http://www.facebook.com/sharer/sharer.php?app_id=1169328043100769&sdk=joey&u=' + url + '&display=popup&ref=plugin&src=share_button',
            'google-plus': 'https://plus.google.com/share?url=' + url,
          }

        if (socialNetwork == 'twitter') {

          //getApiKey in https://console.developers.google.com	
          var apiKey = "AIzaSyDQ9P6bEDsVwkI-Q711tnTt75wYsU5cmzw";

          shortUrl(apiKey, url).then(function(response) {
            shortenUrl = response.data.id;
            socialNetworksURL['twitter'] = "https://twitter.com/share?url=" + shortenUrl + "&text=" + escape("@desafiodaordem " + self.title + ' ')

            launchShareWindow();
          })

        } else {
          launchShareWindow();
        }

        function launchShareWindow() {
          return !$window.open(socialNetworksURL[socialNetwork], socialNetwork, 'width=640,height=580')
        }


      }

      this.whatsapp = function() {
        console.log(self.url + escape(' ' + self.title + ' '));
        return self.url + escape(' ' + self.title + ' ');
      }

      function getURL() {
        var url;

        if (self.relativeUrl) {
          url = 'http://' + $document[0].domain + self.relativeUrl
        } else if (self.url) {
          url = self.url;
        } else {
          url = $document[0].URL;
        }

        return url;
      }

    }
  }
}])
