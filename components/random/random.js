angular.module('random', [])

.factory('randomFunction', ['$interval', function($interval) {
    return function(array, fn, periodicity) {
        var index = 0;

        $interval(function() {
            if (index == array.length) {
                index = 0;
            }

            fn(array[index]);

            index++;
        }, periodicity)
    }
}])

.directive('randomTip', ['randomFunction', function(randomFunction) {
    return {
        restrict: 'C',
        link: function(scope, element) {

            var tips = [
                'Reserve um tempo diário',
                'Além de ler, escreva',
                'Revise o que estudou',
                'Desconecte-se das distrações',
                'Use marca-texto',
                'Dê uma pausa',
                'Exercite-se regularmente',
                'Tenha foco',
                'Seja confiante',
                'Tenha disciplina'
            ]

            ;

            randomFunction(tips, function(item) {
                element.html(item);
            }, 8000);

        }
    }
}])

.directive('randomImage', ['randomFunction', function(randomFunction) {
    return {
        restrict: 'C',
        link: function(scope, element) {

            var folder = 'wp-content/themes/desafio-da-ordem/images/',
                images = [
                    folder+'person_1.png',
                    folder+'person_2.png',
                    folder+'person_3.png'
                ]

            ;

            var index = Math.floor(Math.random()*(images.length));


            element.attr('src',images[index]);
            


            

        }
    }
}])
