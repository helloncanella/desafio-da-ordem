<!DOCTYPE html>
<html ng-app="app" >
<head>
	<base href="/">
	<title>Desafio da Ordem</title>
	<link rel="icon" href="http://desafiodaordem.com.br/wp-content/themes/desafio-da-ordem/images/logo.png">
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">


	<meta charset="UTF-8">
	<meta property="og:url" content="http://desafiodaordem.com.br"/>
	<meta property="og:title" content="Desafio da Ordem" />
	<meta property="og:description" content="Prepare-se para OAB se divertindo" />
	<meta property="og:image" content="http://desafiodaordem.com.br/wp-content/themes/desafio-da-ordem/images/logo.png" />
	<meta property="og:type" content="article" />

	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#00AFF0">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#00AFF0">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="#00AFF0">
	
	<?php wp_head(); ?>
</head>
<body>
	
	<?php 
		if(isset($_POST['contact']) && isset($_POST['submit'])){
			
			//Persisting message from form
			$email = $_POST['email'];
			$name = $_POST['name'];

			$message = 
			"	
				<b>Nome: </b>".$_POST['name']."<br>
				<b>Email: </b>".$_POST['email']."<br>
				<b>Mensagem: </b>".$_POST['message']."<br>		
			";

			$subject = "Mensagem - Desafio da Ordem";

			wp_mail("aplicativo.desafiodaordem@gmail.com", $subject, $message);


			//Sending email to client
			$message =
			"	
				Oi ".$_POST['name'].",<br><br>

				Agradecemos o seu contato.<br> 
				Em algumas horas retornaremos a sua solicitação.<br><br>		
			
				Atenciosamente<br><br>

				<b>Equipe Desafio da Ordem</b>

			";

			$subject = "Desafio da Ordem";

			wp_mail($email, $subject, $message);				
		} 
	?>

	<?php
		if(isset($_POST['mailchimp']) && isset($_POST['submit'])){
			$email = $_POST['email'];
			$fname = $_POST['fname'];

			mailchimp($fname, $email);
		}


		function mailchimp($fname,$email){

			$apikey='e92dd85df2f66e17e2b0845e4f3d5bbe-us13';
			$list_id = '0a2da0c39c';
			$server = 'us13';

			$auth = base64_encode( 'user:'.$apikey );
			$data = array(
				'apikey'        => $apikey,
				'email_address' => $email,
				'status'        => 'subscribed',
				'merge_fields'  => array(
					'FNAME' => $fname
				)
			);

			$json_data = json_encode($data);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
			curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
			$result = curl_exec($ch);

		}
			

	?> 
	
	<div ng-view></div>

	
</body>
</html>
