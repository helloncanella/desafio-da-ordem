var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();

gulp.task('browser-sync', function() {
  browserSync.init({
    notify: false,
    port:'6570',
    proxy: "http://local.wordpress-trunk.dev"
  });
});

gulp.task('production', ['sass'], function(errFunction) {
  $.sequence('copy-for-production', 'minify-css', 'digital-ocean', errFunction);
});

gulp.task('digital-ocean', function() {
  var hostIp = '159.203.77.227';
  return gulp.src('.').pipe($.shell(['scp -r production/* root@' + hostIp + ':/var/www/html/wp-content/themes/desafio-da-ordem']));
});

gulp.task('minify-css', function() {
  gulp.src('./production/style.css')
    .pipe($.uglifycss())
    .pipe(gulp.dest('./production'));
});

gulp.task('copy-for-production', function() {
  gulp.src(['index.php', 'style.css', 'functions.php', './js/scripts.js', 'vendors/**/*', 'components/**/*','images/**/*','typography/**/*'])
    .pipe($.copy('./production', {}));
});


gulp.task('sass', function() {

  return gulp
    .src('./components/bundle.scss',{base:'./'})
    .pipe($.sassGlob())
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer({
      // browsers: ['iOS > 7', 'ie >= 11', 'and_chr >= 2.3'],
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('.'))
    .pipe(browserSync.stream());
});

gulp.task('jade', function() {

  var src = [
      'components/principal/**/*.jade','components/news/**/*.jade','components/cards/**/*.jade',
    'components/services/**/*.jade','components/successful_message/**/*.jade','components/mailchimp/**/*.jade',
    'components/contact/contact-page.jade'
  ];
  gulp.src(src,{base:'components'})
      .pipe($.jadePhp({
        locals: {
          imageFolder: 'wp-content/themes/desafio-da-ordem/images'
        },
        pretty: true
      }))
      .on('error', $.util.log)
      .pipe(gulp.dest('./components'))
      .pipe(browserSync.stream());
});

gulp.task('default', ['sass', 'jade','browser-sync'], function() {
  gulp.watch(['**/*.scss'], ['sass']);
  gulp.watch('**/*.jade', ['jade']);
  gulp.watch(['components/**/*.js','js/scripts.js']).on('change', browserSync.reload);
});


function errFunction(err, data) {
  if (err) console.log(err);
}
