<?php


function load_styles(){

	wp_register_style(
		'icons',
		get_template_directory_uri().'/typography/icons/css/fontello.css'
	);

	wp_register_style(
		'font-awesome',
		get_template_directory_uri().'/vendors/font-awesome/css/font-awesome.min.css'
	);

	wp_register_style(
		'owl',
		get_template_directory_uri().'/vendors/owl.carousel/assets/owl.carousel.min.css'
	);

	wp_enqueue_style(
		'bundleCss',
		get_template_directory_uri().'/components/bundle.css',
		array('font-awesome','icons','owl')
	);
}


function load_scripts() {

	wp_register_script(
		'lodash',
		get_stylesheet_directory_uri() . '/vendors/lodash/dist/lodash.min.js'
	);

	wp_register_script(
		'masonry',
		get_stylesheet_directory_uri() . '/vendors/masonry/dist/masonry.pkgd.min.js'
	);

	wp_register_script(
		'cards',
		get_stylesheet_directory_uri() . '/components/cards/cards.js'
	);

	

	wp_register_script(
		'DOMServiceModule',
		get_stylesheet_directory_uri() . '/components/services/DOMServiceModule.js'
	);

	wp_register_script(
		'share',
		get_stylesheet_directory_uri() . '/components/share/share.js'
	);


	wp_register_script(
		'downloadButton',
		get_stylesheet_directory_uri() . '/components/download/download.js'
	);
	

	wp_register_script(
		'jquery',
		get_stylesheet_directory_uri() . '/vendors/jquery/dist/jquery.min.js'
	);

	wp_register_script(
		'owl',
		get_stylesheet_directory_uri() . '/vendors/owl.carousel/owl.carousel.min.js'
	);

	wp_register_script(
		'angularjs',
		get_stylesheet_directory_uri() . '/vendors/angular/angular.min.js'
	);

	wp_register_script(
		'carrossel',
		get_stylesheet_directory_uri() . '/components/carrossel/carrossel.js'
	);

	wp_register_script(
		'shortnerUrl',
		get_stylesheet_directory_uri() . '/components/shortnerUrl/shortnerUrl.js'
	);

	wp_register_script(
		'random',
		get_stylesheet_directory_uri() . '/components/random/random.js'
	);

	wp_register_script(
		'angularjs-route',
		get_stylesheet_directory_uri() . '/vendors/angular-route/angular-route.min.js'
	);
	wp_register_script(
		'angularjs-sanitize',
		get_stylesheet_directory_uri() . '/vendors/angular-sanitize/angular-sanitize.min.js'
	);

	wp_register_script(
		'angularjs-sanitize',
		get_stylesheet_directory_uri() . '/vendors/angular-sanitize/angular-sanitize.min.js'
	);

	wp_enqueue_script(
		'my-scripts',
		get_stylesheet_directory_uri() . '/js/scripts.js',
		array( 'jquery','owl','angularjs', 'angularjs-route', 'angularjs-sanitize', 'lodash', 'masonry','DOMServiceModule','shortnerUrl', 'share','cards','random','downloadButton')
	);
	wp_localize_script(
		'my-scripts',
		'root',
		array(
			'components' => trailingslashit( get_template_directory_uri() ) . 'components/'
			)
	);
}

function my_admin_bar_init() {
	//Removendo estilo imposto pelo wordpress
	//retirado de: https://wordpress.org/support/topic/margin-top-32px
	remove_action('wp_head', '_admin_bar_bump_cb');
}

	

function configure_smtp( PHPMailer $phpmailer ){
    $phpmailer->isSMTP(); //switch to smtp
    $phpmailer->Host = 'smtp.gmail.com';
    $phpmailer->SMTPAuth = true;
    $phpmailer->SMTPSecure = 'ssl';
    $phpmailer->Port = 465;
    $phpmailer->Username = 'aplicativo.desafiodaordem@gmail.com';
    $phpmailer->Password = 'senhadesafio123';
    $phpmailer->SetFrom("aplicativo.desafiodaordem@gmail.com");
    $phpmailer->FromName='Desafio da Ordem';
    // $phpmailer->SMTPDebug=2;
}


add_action( 'phpmailer_init', 'configure_smtp' );

add_action('admin_bar_init', 'my_admin_bar_init');
add_action( 'wp_enqueue_scripts', 'load_scripts' );
add_action( 'wp_enqueue_scripts', 'load_styles' );

function set_contenttype($content_type){
	return 'text/html';
}

add_filter('wp_mail_content_type','set_contenttype');

