angular.module('DOMServiceModule',[])

.factory('DOMService', function(){

  var factory = {};

  factory.getNumericalStyles = function (element, styles){
    var object = {};

    styles.forEach(function(style){
      object[style] =element[0].style[style]|| window.getComputedStyle(element[0], null)[style];
    });

    for(var key in object){
      if(object[key]){
        object[key] = Number(object[key].match(/\d{1,}/)[0]);
      }
    }

    return object;
  };

  factory.resizeComponent = function(component, ratio){
    var numericalObject = factory.getNumericalStyles(component, ['height', 'width']);

    component.css({
      'height':numericalObject.height*ratio+'px',
      'width': numericalObject.width*ratio+'px'
    });

    return component;
  };


  return factory;
})


;
