angular.module('portaRetrato', ['DOMServiceModule'])

.directive('portaRetrato', ['DOMService', function(DOMService) {
  return {
    link: function(scope, element) {

      var elDim, childContentDim;

      var childContent = element.children();

      childContent.css('position','relative');

      scope.$watch(function() {
        return DOMService.getNumericalStyles(element,['height','width']);
      }, verifyDifference, true);

      function verifyDifference(newValue, old){

        if (newValue !== old) {

          //Element's and child's dimmension;
          elDim = newValue;
          childContentDim = DOMService.getNumericalStyles(childContent,['height','width']);



          //Element-Child ratio
          var ratio = {
            height: elDim.height/childContentDim.height,
            width: elDim.width/childContentDim.width
          };

          // Choosing the largest radio. It guarantee the child's new dimmension will be the biggest
          var choosenRatio = (ratio.height<ratio.width) ? ratio.width : ratio.height;

          childContent.css({
            height:childContentDim.height*choosenRatio+'px',
            width:childContentDim.width*choosenRatio+'px'
          });

          centralizeContent();
        }
      }

      function centralizeContent(){

        var newContentDimmensions = DOMService.getNumericalStyles(childContent, ['height', 'width']);

        var top = (newContentDimmensions.height - elDim.height)/2;
        var left = (newContentDimmensions.width - elDim.width)/2;

        childContent.css({
          top: -top+'px',
          left: -left+'px'
        });
      }
    }
  };
}])
;
