
<div id="articles" ng-controller="CardsController" class="cards">
  <div class="grid column center">
    <categories class="s-full"><span ng-repeat="category in categories" ng-class="selectedCategories[category.id] ? 'selected' : ''" ng-click="click(category.id); search('category',selectedCategories)" class="category small-font">{{category.name}}  </span>
      <input placeholder="Buscar" ng-model="expression" ng-change="search('expression', expression);" class="s-4 search small-font"/>
    </categories>
    <div class="cards hide-for-570-up">     <a ng-repeat="post in posts" ng-href="/noticia/{{post.slug}}/{{post.id}}">
        <div class="card grid row">
          <picture-frame src="images[post.id]" class="s-4"></picture-frame>
          <div class="s-8 article-info">  
            <h4 class="title">{{post.title.rendered}}  </h4>
          </div>
        </div></a></div>
  </div>
  <masonry class="grid show-for-570-up">     
    <div class="grid-sizer"></div>
    <div class="gutter"></div>
    <div ng-repeat="post in posts" monitor="" class="item">
      <h4 class="title">{{post.title.rendered}}</h4><a ng-href="/noticia/{{post.slug}}/{{post.id}}"><img ng-if="images[post.id]" ng-src="{{images[post.id]}}"/></a>
      <div class="footer"><span class="date">{{post.date | date:'dd/MM/yyyy'}}</span><span class="social">
          <share title="{{post.title.rendered}}" relative-url="/noticia/{{post.slug}}/{{post.id}}"><a ng-click="shareCtrl.click('facebook')">
              <div class="facebook"><i class="fa fa-facebook"></i></div></a><a ng-click="shareCtrl.click('twitter')">
              <div class="twitter"><i class="fa fa-twitter"></i></div></a></share></span></div>
    </div>
  </masonry>
</div>