var promises = [];

var app = angular.module('app', ['ngRoute', 'ngSanitize', 'DOMServiceModule', 'cards', 'shareModule', 'downloadModule', 'random']);

app

  .config(['$routeProvider','$locationProvider','$compileProvider',function($routeProvider, $locationProvider, $compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|tel|whatsapp):/);  
      $locationProvider.html5Mode(true);
  
      $routeProvider.when('/', {
        templateUrl: root.components + 'principal/principal.php',
      })
  
      .when('/noticia/:slug/:id', {
          templateUrl: root.components + 'news/news.php',
          controller: ['$scope', '$routeParams', '$http', '$window', '$document', function($scope, $routeParams, $http, $window, $document) {
            var id = $routeParams.id;
  
  
            $http.get('/wp-json/wp/v2/posts/' + id).then(function(response) {
              var article = response.data;
  
              $scope.content = article.content.rendered;
              $scope.url = $document[0].URL;
              $scope.title = article.title.rendered;
              $scope.description = 'Mais um artigo do Desafio da Ordem';
  
              $window.scrollTo(0, 0);
            });
  
          }]
        })
        .when('/sucesso', {
          templateUrl: root.components + 'successful_message/successful_message.php',
        })
  
      .when('/mailchimp', {
        templateUrl: root.components + 'mailchimp/mailchimp.php',
  
      })
  
      .when('/inscricao', {
        templateUrl: root.components + 'mailchimp/subscribe.php',
        controller: ['$window', function($window) {
          $window.scrollTo(0, 0);
        }]
      })
  
      .when('/contato', {
        templateUrl: root.components + 'contact/contact-page.php',
        controller: ['$window', function($window) {
          $window.scrollTo(0, 0);
        }]
      })
  
      .otherwise('/')
  
      ;
    }])

.filter('escape', function() {
  return function(input) {
    if (input) {
      return window.encodeURIComponent(input);
    }
    return "";
  }
})

.controller('historyController', ["$scope","$window", function($scope,$window){
  $scope.historyBack = function(){
    $window.history.back();
  }
}])

.factory('_', function() {

  window._.mixin({
    move: function(array, fromIndex, toIndex) {
      array.splice(toIndex, 0, array.splice(fromIndex, 1)[0]);
      return array;
    }
  });

  return window._;
})



;
